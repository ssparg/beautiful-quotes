package com.shaunsparg.quotes.simplebeautifulquotes;

import java.io.IOException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Shaun on 2018/01/07.
 */

public class GetQuote {
    private OkHttpClient client = new OkHttpClient();

    String post(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
