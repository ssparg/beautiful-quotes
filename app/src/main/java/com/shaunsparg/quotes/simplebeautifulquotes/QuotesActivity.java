package com.shaunsparg.quotes.simplebeautifulquotes;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONObject;

import java.util.Random;

public class QuotesActivity extends AppCompatActivity {

    private static final String QUOTETAG = "QUOTE_TEXT";
    private static final String AUTHORTAG = "AUTHOR_TEXT";
    private static String LOGGER = "SIMPLE_BEAUTIFUL_QUOTES";
    private FirebaseAnalytics mFirebaseAnalytics;
    private String quoteText;
    private String quoteAuthor;
    private ProgressBar progressBar;
    private TextView quoteTextView;
    private TextView authorTextView;
    private TextView author;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotes);

        progressBar = findViewById(R.id.pbHeaderProgress);
        quoteTextView = findViewById(R.id.quoteTextView);
        authorTextView = findViewById(R.id.authorTextView);
        author = findViewById(R.id.textView4);

        SharedPreferences sharedPreferences = getApplication().getSharedPreferences("com.shaunsparg.quotes", 0);
        String backgroundColor = sharedPreferences.getString("color", "#FFFFFF");
        ConstraintLayout constraintLayout = findViewById(R.id.quotes_layout);
        constraintLayout.setBackgroundColor(Color.parseColor(backgroundColor));
        initializeAds();
        if (savedInstanceState != null) {
            quoteText = savedInstanceState.getString(QUOTETAG);
            quoteAuthor = savedInstanceState.getString(AUTHORTAG);
            setQuoteAuthorText();
        } else {
            getNewQuote();
        }

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


    }

    private void getNewQuote() {
        progressBar.setVisibility(View.VISIBLE);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                GetQuote quote = new GetQuote();
                try {
                    String response = quote.post(getString(R.string.quote_get_url));
                    JSONObject json = new JSONObject(response);
                    quoteText = json.getString("quoteText");
                    quoteAuthor = json.getString("quoteAuthor");
                    if (quoteAuthor.isEmpty()) {
                        quoteAuthor = getString(R.string.unknown_author);
                    }
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            setQuoteAuthorText();
                        }
                    });

                } catch (Exception e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            setOfflineQuote();
                            errorNotification();
                        }
                    });
                }
            }
        };
        new Thread(runnable).start();
    }

    private void errorNotification() {
        Toast.makeText(this, R.string.get_quote_error, Toast.LENGTH_LONG).show();
    }

    private void initializeAds() {
        MobileAds.initialize(this, getString(R.string.admob_key));
        AdView adView = findViewById(R.id.quote_banner_ad);
        AdRequest adRequest = new AdRequest.Builder().build();

        adView.loadAd(adRequest);
    }

    private String[][] buildOfflineQuotes() {
        String[][] offlineQuotes = {
                {"Ralph Emerson", "Do not go where the path may lead, go instead where there is no path and leave a trail. "},
                {"Mark Twain", "There are basically two types of people. People who accomplish things, and people who claim to have accomplished things. The first group is less crowded. "},
                {"Winston Churchill", "I never worry about action, but only inaction."},
                {"Aristotle", "Criticism is something you can easily avoid by saying nothing, doing nothing, and being nothing."},
                {"Buddha", "We are formed and moulded by our thoughts. Those whose minds are shaped by selfless thoughts give joy when they speak or act. Joy follows them like a shadow that never leaves them."},
                {"Blaise Pascal", "The heart has its reasons which reason knows not of."},
                {"Oliver Wendell Holmes", "A man may fulfil the object of his existence by asking a question he cannot answer, and attempting a task he cannot achieve."},
                {"William Shakespeare", "Go to your bosom: Knock there, and ask your heart what it doth know."},
                {"Oprah Winfrey", "With every experience, you alone are painting your own canvas, thought by thought, choice by choice."},
                {"Wayne Dyer", "You cannot be lonely if you like the person you're alone with."}
        };

        return offlineQuotes;

    }

    private void setOfflineQuote() {
        String[][] offlineQuotes =  buildOfflineQuotes();
        Random random = new Random();
        int low = 1;
        int high = offlineQuotes.length;
        int randomNumber = random.nextInt(high-low) + low;
        quoteAuthor = offlineQuotes[randomNumber][0];
        quoteText = offlineQuotes[randomNumber][1];
        setQuoteAuthorText();
        }

    private void setQuoteAuthorText() {
        quoteTextView.setText(quoteText);
        authorTextView.setText(quoteAuthor);
        quoteTextView.setVisibility(View.VISIBLE);
        authorTextView.setVisibility(View.VISIBLE);
        author.setVisibility(View.VISIBLE);
    }

    private void setQuoteAuthorTextHidden() {
        quoteTextView.setVisibility(View.INVISIBLE);
        authorTextView.setVisibility(View.INVISIBLE);
        author.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (quoteText != null) {
            outState.putString(QUOTETAG, quoteText);
        }
        if(quoteAuthor != null) {
            outState.putString(AUTHORTAG, quoteAuthor);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                setQuoteAuthorTextHidden();
                getNewQuote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
