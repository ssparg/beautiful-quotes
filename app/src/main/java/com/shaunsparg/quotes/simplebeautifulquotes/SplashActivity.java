package com.shaunsparg.quotes.simplebeautifulquotes;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.Random;

import gr.net.maroulis.library.EasySplashScreen;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String res[] = getResources().getStringArray(R.array.background_colors);
        String backgroundcolor = setBackgroundColor(res);
        SharedPreferences sharedPreferences = getApplication().getSharedPreferences("com.shaunsparg.quotes", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("color", backgroundcolor);
        editor.apply();
        EasySplashScreen config = new EasySplashScreen(SplashActivity.this)
                .withFullScreen()
                .withTargetActivity(QuotesActivity.class)
                .withSplashTimeOut(5000)
                .withBackgroundColor(Color.parseColor(backgroundcolor))
                .withLogo(R.drawable.splash_icon)
                .withAfterLogoText(getString(R.string.app_name));

        config.getAfterLogoTextView().setTextColor(Color.parseColor("#ffffff"));
        config.getAfterLogoTextView().setTextSize(24);
        View view = config.create();

        setContentView(view);
    }

    private String setBackgroundColor(String[] items) {
        Random random = new Random();
        int low = 1;
        int high = items.length;
        int randomNumber = random.nextInt(high-low) + low;
        return items[randomNumber];
    }
}
